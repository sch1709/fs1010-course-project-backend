import express from 'express'
import * as db from '../util/dataHandler'
import * as helpers from '../util/helpers'
import {v4 as uuid} from 'uuid'

const router = express.Router()

const validateEntries = (req, res ,next) => {
    const error = helpers.validateForm(["email", "username", "password"], req.body)
    if(error.length > 0){
        return res.status(400).send({message: "validation error", invalid})
    }
    next()
}

router.post('/', validateEntries, async (req, res) => {
    const duplicate = await helpers.validateDuplicate(req.body)
    if(duplicate[0] === "mail" || duplicate[0] === "username"){
        return res.status(409).send(JSON.stringify(duplicate[0]))
    }else{
        helpers.hashPassword(req.body.password).then(hash => {
            const newUser = {
                id: uuid(),
                email: req.body.email,
                username: req.body.username,
                password: hash
            }
            db.add(process.env.PATH_USERS, newUser)
            return res.send(newUser)
        })
    }
})


export default router;