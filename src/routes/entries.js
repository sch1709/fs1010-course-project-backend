import express from 'express'
import jwt from 'express-jwt'
import * as db from '../util/dataHandler'
import * as helpers from '../util/helpers'
import {v4 as uuid} from 'uuid'

const router = express.Router()

const validateEntries = (req, res, next) => {
    const error = helpers.validateForm(["name", "email", "phoneNumber", "content"], req.body)
    if(error.length > 0){
        return res.status(400).send({message: "validation error", invalid})
    }
    next()
}

router.post('/', validateEntries, async (req, res) => {
    const body = req.body
    const newEntry = {id: uuid(), ...body}
    await db.add(process.env.PATH_ENTRIES, newEntry)
    return res.send(newEntry) 
})

router.use(jwt({secret: process.env.JWT_SECRET}))

router.get('/', async (req, res) => {
    res.send(await db.getAll(process.env.PATH_ENTRIES))
})

export default router
