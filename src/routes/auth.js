import express from 'express'
import * as jwtGenerator from 'jsonwebtoken'
import * as helpers from '../util/helpers'
import * as db from '../util/dataHandler'

const router = express.Router()

router.post('/', async (req, res) => {
    const username = req.body.username
    const password = req.body.password 

    if (username == "test" && password == "password") {
        const token = jwtGenerator.sign({username}, process.env.JWT_SECRET, {expiresIn: '1h'})
        return res.send({token})
    }else{
        let users = await db.getAll(process.env.PATH_USERS)
        const userFound = users.find(user => user.username == username)
        if(userFound == undefined || userFound.length == 0){
            return res.status(401).send({error: "incorrect username\password"})
        }else{
            helpers.verifyPassword(password, userFound.password).then(valid => {
                if(valid == false){
                    return res.status(401).send({error: "incorrect username\password"})
                }else{
                    const token = jwtGenerator.sign({username}, process.env.JWT_SECRET, {expiresIn: '1h'})
                    return res.send({token})
                }
            })
        }
    }
})

export default router