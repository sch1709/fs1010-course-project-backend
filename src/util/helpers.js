import * as argon from 'argon2'
import * as db from './dataHandler'

//COMPLETE FORM VALIDATION
const validateForm = (properties, body) => {
    const invalid = []
    for (var property in body) {
        if (body[property] == null || body[property].length === 0) {
            invalid.push(property)
        }
    }
    const requiredProperties = properties
    requiredProperties.filter(prop => !body.hasOwnProperty(prop)).forEach(key => invalid.push(key))
    return invalid
}

//VALIDATE NO DUPLICATE NEW USER
const validateDuplicate = async(user) => {
    const invalid = []
    const email = user.email
    const username = user.username
    let usersDb = await db.getAll(process.env.PATH_USERS)
    const userExistbyMail = usersDb.filter(user => user.email == email)
    const userExistbyUsername = usersDb.filter(user => user.username == username)

    if(userExistbyMail.length > 0 ){
        invalid.push("mail")
    }else if(userExistbyUsername.length > 0 ){
        invalid.push("username")
    }
    return invalid
}

//HASH PASSWORD
const hashPassword = async(password) => {
    try{
        const hash = await argon.hash(password)
        return hash
    }catch(err){
        console.error(err)
    }
}

//VERIFY PASSWORD
const verifyPassword = async(password, hash) => {
    try{
        const match = await argon.verify(hash, password)
        return match
    }catch(err){
        console.error(err)
    }
}

export {
    validateForm,
    validateDuplicate,
    hashPassword,
    verifyPassword
}