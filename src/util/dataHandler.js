import { promises as fs } from 'fs'
import path from 'path'
// import { isArray } from 'util'

// private function for the module
const write = async (url, data) => {
    await fs.writeFile(url, JSON.stringify(data, null, 2))
}

const add = async (url, data) => {
    const file = path.resolve(url)
    try {
        let content = await getAll(file)
        content.push(data)
        write(url, content)
        console.log("file written")
    } catch (err) {
        console.error(err)
        throw err
    }

}

const getAll = async (url) => {
    const file = path.resolve(url)
    try {
        let content = await fs.readFile(file)
        return JSON.parse(content)
    } catch (err) {
        let content = []
        return content
    }
}

// const update = async (id, data) => {
//     let content = await getAll()
//     if (!isArray(content)) {
//         throw new Error("No data found")
//     }

//     const itemLocation = content.findIndex(item => item.id=== id)
//     console.log(id)
//     if (itemLocation != -1) {
//         content[itemLocation] = data
//     } else {
//         throw new Error(`ID: ${id} not found`)
//     }

//     // let's write it back to the file now
//     write(content)
// }

export {
    add,
    getAll,
    // update
}
