import express from 'express'
import entryRoutes from './src/routes/entries'
import entryAuth from './src/routes/auth'
import entryUsers from './src/routes/users'
import dotenv  from 'dotenv'
import cors from 'cors'

dotenv.config();
const app = express()
const PORT = process.env.PORT || 3000

app.use(express.json())
app.use(cors())

app.use('/auth', entryAuth)
app.use('/users', entryUsers)
app.use('/contact_form/entries', entryRoutes)

app.listen(PORT, () => {
    console.log(`Server started at http://localhost:${PORT}`)
})
